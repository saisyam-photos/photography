+++
title = "Spot Bill Pelican"
description = "Spotted at Ranganathittu bird sanctuary"

date = "2020-02-07"
categories = "birds"
tags = [
    "spot-bill-pelican",
]
image = "spot-bill-pelican.jpg"
+++