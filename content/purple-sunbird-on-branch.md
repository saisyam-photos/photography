+++
title = "Purple Sunbird"
description = "Spotted at Banjara hills, Hyderabad"

date = "2020-03-09"
categories = "birds"
tags = [
    "purple-sunbird",
]
image = "purple-sunbird-on-branch.jpg"
+++