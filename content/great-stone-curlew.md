+++
title = "Great Stone Curlew"
description = "Spotted at Ranganathittu bird sanctuary"

date = "2020-02-07"
categories = "birds"
tags = [
    "great-stone-curlew",
]
image = "great-stone-curlew.jpg"
+++