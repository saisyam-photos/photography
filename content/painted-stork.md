+++
title = "Painted Stork"
description = "Spotted at Ameenpur Lake"

date = "2020-03-17"
categories = "birds"
tags = [
    "painted-stork",
]
image = "painted-stork.jpg"
+++