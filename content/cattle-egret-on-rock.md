+++
title = "Cattle Egret"
description = "Spotted at Ameenpur Lake"

date = "2020-03-17"
categories = "birds"
tags = [
    "cattle-egret",
]
image = "cattle-egret-on-rock.jpg"
+++