+++
title = "Green Bee Eater"
description = "Spotted at Ameenpur Lake"

date = "2020-03-17"
categories = "birds"
tags = [
    "green-bee-eater",
]
image = "green-bee-eater-with-flower.jpg"
+++