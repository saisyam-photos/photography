+++
title = "Asian Openbill Stork"
description = "Spotted at Ranganathittu bird sanctuary"

date = "2020-02-07"
categories = "birds"
tags = [
    "asian-openbill-stork",
]
image = "asian-openbill-stork.jpg"
+++