+++
title = "Purple Heron"
description = "Spotted at Ameenpur Lake"

date = "2020-03-17"
categories = "birds"
tags = [
    "purple-heron",
]
image = "purple-heron-behind-grass.jpg"
+++