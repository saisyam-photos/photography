+++
title = "Overexposed Rose Flower"
description = "Shot in my balcony. Overexposed but beautiful"

date = "2020-03-15"
categories = "flowers"
tags = [
    "rose-flower",
]
image = "overexposed-rose-flower.jpg"
+++